<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/fprasetyo4/todolist-app">
    <img src="https://res.cloudinary.com/dbpfwb5ok/image/upload/v1662775866/recipe/todoapp/logo_vgllxy.jpg" alt="Logo" width="150px">
  </a>

  <h3 align="center">Todo App</h3>

  <p align="center">
    List your Activity
  </p>
</div>

<!-- TABLE OF CONTENTS -->

## Table of Contents

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
        <li><a href="#setup-env-example">Setup .env example</a></li>
      </ul>
    </li>
    <li><a href="#screenshoots">Screenshots</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#related-project">Related Project</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#license">License</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

**Todo App** is a website that has main feauture that you can listing your dailiy activity.

### Built With

This app was built with some technologies below:

- [CSS](https://developer.mozilla.org/en-US/docs/Web/CSS)
- [Javascript](https://www.javascript.com/)
- [Nuxt JS](https://nuxtjs.org/)
- [Axios](https://axios-http.com/)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

### Prerequisites

Before going to the installation stage there are some software that must be installed first.

- [NodeJs](https://nodejs.org/en/download/)

<p align="right">(<a href="#top">back to top</a>)</p>

### Installation

If you want to run this project locally, I recommend you to configure the [back-end](https://github.com/fandipras7/telegram_api) first before configuring this repo front-end.

- Clone the repo

```
git clone https://gitlab.com/fprasetyo4/todolist-app.git
```

- Go To Folder Repo

```
cd todo-app
```

- Install Module

```
npm install
```

- <a href="#setup-env">Setup .env</a>
- Type ` npm run dev` To Start Website
- Type ` npm run start` To Start Production

<p align="right">(<a href="#top">back to top</a>)</p>

<p align="right">(<a href="#top">back to top</a>)</p>

## Screenshoots

<p align="center" display=flex>
   
<table>
 
  <tr>
    <td><img src="https://res.cloudinary.com/dbpfwb5ok/image/upload/v1662775732/recipe/todoapp/todo-app_pe6odx.png" alt="Logo" width=100%></td>
    <td><img src="https://res.cloudinary.com/dbpfwb5ok/image/upload/v1662775732/recipe/todoapp/login_ayzv9e.png" width=100%/></td>
  </tr>
   <tr>
    <td>Home Page</td>
    <td>Login Page</td>
  </tr>
  <tr>
    <td><img src="https://res.cloudinary.com/dbpfwb5ok/image/upload/v1662775732/recipe/todoapp/register_ofipg2.png" alt="Chat List" width=100%></td>
    <td><img src="https://res.cloudinary.com/dbpfwb5ok/image/upload/v1662775732/recipe/todoapp/edit_eeoayl.png" alt="Menu" width=100%/></td>
  </tr>
  <tr>
    <td>Register</td>
    <td>Edit</td>
  </tr>
</table>
      
</p>
<p align="right">(<a href="#top">back to top</a>)</p>

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>


<p align="right">(<a href="#top">back to top</a>)</p>

## Contact

My Email : fprasetyo4@gmail.com

<p align="right">(<a href="#top">back to top</a>)</p>

## License

Distributed under the [MIT](/LICENSE) License.

<p align="right">(<a href="#top">back to top</a>)</p>
